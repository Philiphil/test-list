<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController{

	/**
	 *@Route("/", name="index")
	 */
	public function index(){
		return $this->render('default/index.html.twig',
			["page_title" => "Index"]
		);
	}

	/**
	 *@Route("/app", name="app")
	 *
	 */
	public function app(){
		return $this->render('default/app.html.twig',
			["page_title" => "Index"]
		);
	}
}