<?php
// api/src/Entity/ShoppingList.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
/**
 * A ShoppingList.
 *
 * @ORM\Entity
 * @ApiResource(attributes={"filters"={"ShoppingList.User"}})
 *     
 */
class ShoppingList
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column
     */
    public $title='';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="shoppingLists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShoppingItem", mappedBy="shoppingList", orphanRemoval=true)
     * @ApiSubresource
     */
    private $ShoppingItems;

    
    public function __construct()
    {
        $this->ShoppingItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTitle($title): self
    {
        $this->title=$title;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection|ShoppingItem[]
     */
    public function getShoppingItems(): Collection
    {
        return $this->ShoppingItems;
    }

    public function addShoppingItem(ShoppingItem $shoppingItem): self
    {
        if (!$this->ShoppingItems->contains($shoppingItem)) {
            $this->ShoppingItems[] = $shoppingItem;
            $shoppingItem->setShoppingList($this);
        }

        return $this;
    }

    public function removeShoppingItem(ShoppingItem $shoppingItem): self
    {
        if ($this->ShoppingItems->contains($shoppingItem)) {
            $this->ShoppingItems->removeElement($shoppingItem);
            // set the owning side to null (unless already changed)
            if ($shoppingItem->getShoppingList() === $this) {
                $shoppingItem->setShoppingList(null);
            }
        }

        return $this;
    }

}