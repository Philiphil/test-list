<?php
// api/src/Entity/ShoppingItem.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
/**
 * A ShoppingItem.
 *
 * @ORM\Entity
 * @ApiResource
 */
class ShoppingItem
{
    /**
     * @var int The id of this book.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column
     */
    public $item='';

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    public $checked=false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShoppingList", inversedBy="ShoppingItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shoppingList;

    
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setItem($item): self
    {
        $this->item=$item;
        return $this;
    }

    public function getItem(): ?string
    {
        return $this->item;
    }

    public function setChecked(bool $checked): self
    {
        $this->checked=$checked;
        return $this;
    }

    public function getChecked(): ?bool
    {
        return !!$this->checked;
    }

    public function getShoppingList(): ?ShoppingList
    {
        return $this->shoppingList;
    }

    public function setShoppingList(?ShoppingList $shoppingList): self
    {
        $this->shoppingList = $shoppingList;

        return $this;
    }
}