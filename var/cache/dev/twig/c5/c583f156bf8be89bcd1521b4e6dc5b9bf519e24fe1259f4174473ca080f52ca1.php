<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/index.html.twig */
class __TwigTemplate_5e8eefb6fb6a4449dc0670faf115be9ed291c4c939c45dbddf7f40bdf3d4a874 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
";
        // line 3
        echo twig_include($this->env, $context, "default/includes/head.html.twig");
        echo "
<body>
\t<nav class=\"navbar navbar-expand-md navbar-dark bg-dark fixed-top\">
\t  <a class=\"navbar-brand\" href=\"#\">Listing</a>
\t</nav>


\t<main role=\"main\" class=\"container\">
\t  <div class=\"starter-template\">
\t    <h1>My list</h1>
\t  </div>
\t</main>


";
        // line 17
        echo twig_include($this->env, $context, "default/includes/footer.html.twig");
        echo "
</body>
</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 17,  44 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
{{ include('default/includes/head.html.twig') }}
<body>
\t<nav class=\"navbar navbar-expand-md navbar-dark bg-dark fixed-top\">
\t  <a class=\"navbar-brand\" href=\"#\">Listing</a>
\t</nav>


\t<main role=\"main\" class=\"container\">
\t  <div class=\"starter-template\">
\t    <h1>My list</h1>
\t  </div>
\t</main>


{{ include('default/includes/footer.html.twig') }}
</body>
</html>", "default/index.html.twig", "/home/user/bookshop-api/templates/default/index.html.twig");
    }
}
