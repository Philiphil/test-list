<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_5ea4f15ab37d88ec86887568cb55c5a838c7805ea4acba8c916cf4717057f7f5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
\t";
        // line 3
        echo twig_include($this->env, $context, "default/includes/head.html.twig");
        echo "
\t";
        // line 4
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 5
        echo "<body>
\t<nav class=\"navbar navbar-expand-md navbar-dark bg-dark fixed-top\">
\t  <a class=\"navbar-brand\" href=\"#\">List</a>
\t  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
\t    <span class=\"navbar-toggler-icon\"></span>
\t  </button>

\t  <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">
\t     <ul class=\"navbar-nav mr-auto\">
\t       <li class=\"nav-item\">
\t         <a class=\"nav-link\" href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app");
        echo "\">Home</span></a>
\t       </li>
\t\t";
        // line 17
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 18
            echo "\t    <li class=\"nav-item\">
\t    \t<a class=\"nav-link\" href=\"";
            // line 19
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">Logout</span></a>
\t\t<li>
\t\t";
        } else {
            // line 22
            echo "\t\t<li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" href=\"";
            // line 23
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo "\">LogIn</span></a>
\t\t<li>
\t\t<li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" href=\"";
            // line 26
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_register");
            echo "\">SignUp</span></a>
\t\t<li>
\t\t";
        }
        // line 29
        echo "\t\t\t</ul>
\t\t</div>
\t</nav>

\t";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "\t";
        echo twig_include($this->env, $context, "default/includes/footer.html.twig");
        echo "
\t";
        // line 35
        $this->displayBlock('javascripts', $context, $blocks);
        // line 36
        echo "</body>
</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 33
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 35
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 35,  131 => 33,  119 => 4,  111 => 36,  109 => 35,  104 => 34,  102 => 33,  96 => 29,  90 => 26,  84 => 23,  81 => 22,  75 => 19,  72 => 18,  70 => 17,  65 => 15,  53 => 5,  51 => 4,  47 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
\t{{ include('default/includes/head.html.twig') }}
\t{% block stylesheets %}{% endblock %}
<body>
\t<nav class=\"navbar navbar-expand-md navbar-dark bg-dark fixed-top\">
\t  <a class=\"navbar-brand\" href=\"#\">List</a>
\t  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
\t    <span class=\"navbar-toggler-icon\"></span>
\t  </button>

\t  <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">
\t     <ul class=\"navbar-nav mr-auto\">
\t       <li class=\"nav-item\">
\t         <a class=\"nav-link\" href=\"{{ path('app') }}\">Home</span></a>
\t       </li>
\t\t{% if is_granted('IS_AUTHENTICATED_FULLY') %}
\t    <li class=\"nav-item\">
\t    \t<a class=\"nav-link\" href=\"{{ path('app_logout') }}\">Logout</span></a>
\t\t<li>
\t\t{% else %}
\t\t<li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" href=\"{{ path('app_login') }}\">LogIn</span></a>
\t\t<li>
\t\t<li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" href=\"{{ path('app_register') }}\">SignUp</span></a>
\t\t<li>
\t\t{% endif %}
\t\t\t</ul>
\t\t</div>
\t</nav>

\t{% block body %}{% endblock %}
\t{{ include('default/includes/footer.html.twig') }}
\t{% block javascripts %}{% endblock %}
</body>
</html>", "base.html.twig", "/home/user/bookshop-api/templates/base.html.twig");
    }
}
